const { Router } = require('express')
const { signUp } = require('../controllers/user')

const router = Router()

/**
 * @swagger
 * components:
 *  schemas:
 *    User:
 *      type: object
 *      properties:
 *        name:
 *          type: string
 *          description: Nombre del usuario
 *        lastName:
 *          type: string
 *          description: Apellido del usuario
 *        email:
 *          type: string
 *          description: Email del usuario
 *        password:
 *          type: string
 *          description: Contraseña del usuario
 *        repeatPassword:
 *          type: string
 *          description: Repetir contraseña del usuario
 *      required:
 *        - name
 *        - lastName
 *        - email
 *        - password
 *        - repeatPassword
 *      example:
 *        name: Juan Pablo
 *        lastName: Pachar Viñan
 *        email: jpablo@email.com
 *        password: 123456
 *        repeatPassword: 123456
 *    UserError:
 *      type: object
 *      properties:
 *        msg:
 *          type: string
 *          description: Error al registrar el usuario
 *      example:
 *        msg: Error al registrar el usuario
 */

/**
 * @swagger
 * tags:
 *  name: Users
 *  description: Administración de usuarios
 */

/**
 * @swagger
 * /sign-up:
 *  post:
 *    summary: Registra un nuevo usuario
 *    tags: [Users]
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/User'
 *    responses:
 *      200:
 *        description: El usuario se registró correctamente
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/User'
 *      404:
 *        description: Error al registrar el usuario
 *
 */
router.post('/sign-up', signUp)

module.exports = router
