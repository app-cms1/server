const options = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'CMS API',
      version: '1.0.0',
      description: 'Una api para el manejo de contenido'
    },
    servers: [
      {
        url: process.env.API_URL
      }
    ]
  },
  apis: ['./src/routes/*.js']
}

module.exports = {
  options
}
