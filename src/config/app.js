const cors = require('cors')
const express = require('express')
const morgan = require('morgan')
const swaggerUI = require('swagger-ui-express')
const swaggerJsDoc = require('swagger-jsdoc')
const { options } = require('../docs/swagger')
const userRoutes = require('../routes/user')

const app = express()
const specs = swaggerJsDoc(options)

app.use(cors())
app.use(express.json())
app.use(morgan('dev'))

app.use('/api/users', userRoutes)
app.use('/docs', swaggerUI.serve, swaggerUI.setup(specs))

module.exports = app
